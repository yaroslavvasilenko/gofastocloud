package unit_test

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/fastogt/gofastocloud/media"
)

func TestUnmarshalJSON(t *testing.T) {
	var testJSON []byte

	//type ScanFolderRequest
	testVariable := media.ScanFolderRequest{}
	testJSON = []byte(`{"directory":"directory","extensions":["ext1","ext2"]}`)
	err := json.Unmarshal(testJSON, &testVariable)
	assert.True(t, err == nil)
	assert.Equal(t, testVariable.Directory, "directory")
	assert.Equal(t, testVariable.Extensions, []string{"ext1", "ext2"})

	testJSON = []byte(`{"extensions":["ext1","ext2"]}`)
	err = json.Unmarshal(testJSON, &testVariable)
	assert.Error(t, err)

	testJSON = []byte(`{"directory":"directory"}`)
	err = json.Unmarshal(testJSON, &testVariable)
	assert.Error(t, err)

	// type StopStreamRequest
	testVariable2 := media.StopStreamRequest{}
	testJSON = []byte(`{"id":"id","force":true}`)
	err = json.Unmarshal(testJSON, &testVariable2)
	assert.True(t, err == nil)
	assert.Equal(t, testVariable2.Sid, media.StreamId("id"))
	assert.Equal(t, testVariable2.Force, true)

	//  nil sid/id
	testJSON = []byte(`{"force":true}`)
	err = json.Unmarshal(testJSON, &testVariable2)
	assert.Error(t, err)

	// empty sid/ id
	testJSON = []byte(`{"id":"","force":true}`)
	err = json.Unmarshal(testJSON, &testVariable2)
	assert.Error(t, err)

	//  empty force
	testJSON = []byte(`{"id":"id"}`)
	err = json.Unmarshal(testJSON, &testVariable2)
	assert.Error(t, err)

	// type ChangeInputStreamRequest
	testVariable3 := media.ChangeInputStreamRequest{}
	testJSON = []byte(`{"id":"id","channel_id":2}`)
	err = json.Unmarshal(testJSON, &testVariable3)
	assert.True(t, err == nil)
	assert.Equal(t, testVariable3.Sid, media.StreamId("id"))
	assert.Equal(t, testVariable3.ChannelId, 2)

	//  nil id/sid
	testJSON = []byte(`{"id":"id"}`)
	err = json.Unmarshal(testJSON, &testVariable3)
	assert.Error(t, err)

	//  empty id/sid
	testJSON = []byte(`{"id":"","channel_id":2}`)
	err = json.Unmarshal(testJSON, &testVariable3)
	assert.Error(t, err)

	//  ChannelId nil
	testJSON = []byte(`{"id":"id"}`)
	err = json.Unmarshal(testJSON, &testVariable3)
	assert.Error(t, err)

	// type RestartStreamRequest
	testVariable4 := media.RestartStreamRequest{}
	testJSON = []byte(`{"id":"id"}`)
	err = json.Unmarshal(testJSON, &testVariable4)
	assert.True(t, err == nil)
	assert.Equal(t, testVariable4.Sid, media.StreamId("id"))

	//  nil id/sid
	testJSON = []byte(`{}`)
	err = json.Unmarshal(testJSON, &testVariable4)
	assert.Error(t, err)

	//  empty id/sid
	testJSON = []byte(`{"id":""}`)
	err = json.Unmarshal(testJSON, &testVariable4)
	assert.Error(t, err)

	// type ActivateRequest
	testVariable5 := media.ActivateRequest{}
	testJSON = []byte(`{"license_key":"111111"}`)
	err = json.Unmarshal(testJSON, &testVariable5)
	assert.Error(t, err)

	testVariable6 := media.ActivateRequest{}
	testJSON = []byte(`{"license_key":"0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"}`)
	err = json.Unmarshal(testJSON, &testVariable6)
	assert.True(t, err == nil)

	//  type BaseConfig
	testVariable7 := media.BaseConfig{}
	testJSON = []byte(`{"id":"awdad", "type": 1, "output":[]}`)
	err = json.Unmarshal(testJSON, &testVariable7)
	assert.NoError(t, err)
	assert.Equal(t, testVariable7.Id, media.StreamId("awdad"))
	assert.Equal(t, testVariable7.Type, media.StreamType(1))

	testCases := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "idNil",
			testArray: []byte(`{"type": 1, "output":[]}`),
		},
		{
			name:      "idEmpty",
			testArray: []byte(`{"id":"", "type": 1, "output":[]}`),
		},
		{
			name:      "invalid id",
			testArray: []byte(`{"id":"awdad", "type": -42, "output":[]}`),
		},
		{
			name:      "invalid id",
			testArray: []byte(`{"id":"awdad", "type": 100, "output":[]}`),
		},
		{
			name:      "idNil",
			testArray: []byte(`{"id":"awdad", "output":[]}`),
		},
	}
	for _, obj := range testCases {
		t.Run(obj.name, func(t *testing.T) {
			assert.Error(t, json.Unmarshal(obj.testArray, &testVariable7))
		})
	}

	// type InputUrl
	testVariable13 := media.InputUrl{}
	testJSON = []byte(`{"id":23, "uri": "qwer"}`)
	err = json.Unmarshal(testJSON, &testVariable13)
	assert.NoError(t, err)
	assert.Equal(t, testVariable13.Id, 23)
	assert.Equal(t, testVariable13.Uri, "qwer")

	testCases3 := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "id nil",
			testArray: []byte(`{"uri": "qwer"}`),
		},
		{
			name:      "invlaid id",
			testArray: []byte(`{"id":-234, "uri": "qwer"}`),
		},
		{
			name:      "uri nil",
			testArray: []byte(`{"id":23}`),
		},
		{
			name:      "uri empty",
			testArray: []byte(`{"id":23, "uri": ""}`),
		},
	}

	for _, obj := range testCases3 {
		t.Run(obj.name, func(t *testing.T) {
			assert.Error(t, json.Unmarshal(obj.testArray, &testVariable13))
		})
	}

	// type OutputUrl
	testVariable11 := media.OutputUrl{}
	testJSON = []byte(`{"id":23, "uri": "qwer"}`)
	err = json.Unmarshal(testJSON, &testVariable11)
	assert.NoError(t, err)
	assert.Equal(t, testVariable11.Id, 23)
	assert.Equal(t, testVariable11.Uri, "qwer")

	testCases11 := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "id nil",
			testArray: []byte(`{"uri": "qwer"}`),
		},
		{
			name:      "invlaid id",
			testArray: []byte(`{"id":-234, "uri": "qwer"}`),
		},
		{
			name:      "uri nil",
			testArray: []byte(`{"id":23}`),
		},
		{
			name:      "uri empty",
			testArray: []byte(`{"id":23, "uri": ""}`),
		},
	}

	for _, obj := range testCases11 {
		t.Run(obj.name, func(t *testing.T) {
			assert.Error(t, json.Unmarshal(obj.testArray, &testVariable13))
		})
	}

	// type InputUriData
	testVariable14 := media.InputUriData{}
	testJSON = []byte(`{
	"user_agent": 3,
	"stream_link": {"http_proxy": "wdasd","https_proxy": "fwef","prefer": 2},
	"proxy": "uyioooo","wpe": {"gl": true},
	"program_number": "ewqweqwe",
	"multicast_iface": "efefefef","srt_mode": 1,
	"srt_key": {"passphrase": "qweewq","pbkeylen": 321},
	"rtmpsrc_type": 0,
	"webrtc": {
		"stun": "rewr",
		"turn": "rew"
	},
	"programme": {
		"channel": "wdwdwd"
	}
}`)
	err = json.Unmarshal(testJSON, &testVariable14)
	assert.NoError(t, err)
	assert.Equal(t, *testVariable14.UserAgent, media.UserAgent(3))
	assert.Equal(t, *testVariable14.StreamLink, media.StreamLink{givPoint("wdasd"), givPoint("fwef"), media.QualityPrefer(2)})
	assert.Equal(t, *testVariable14.Proxy, "uyioooo")
	assert.Equal(t, *testVariable14.Wpe, media.Wpe{GL: true})
	assert.Equal(t, *testVariable14.ProgramNumber, *givPoint("ewqweqwe"))
	assert.Equal(t, *testVariable14.MulticastIface, *givPoint("efefefef"))
	assert.Equal(t, *testVariable14.SrtMode, media.SrtMode(1))
	assert.Equal(t, *testVariable14.SrtKey, media.SrtKey{Passphrase: "qweewq", KeyLen: 321})
	assert.Equal(t, *testVariable14.RtmpSrcType, media.RtmpSrcType(0))
	assert.Equal(t, *testVariable14.WebRTC, media.WebRTCProp{Stun: "rewr", Turn: "rew"})
	assert.Equal(t, *testVariable14.Programme, media.Programme{Channel: "wdwdwd"})

	testCases13 := []struct {
		name      string
		testArray []byte
	}{
		{
			name: "not correct field user_agent",
			testArray: []byte(`{"user_agent": 7,"stream_link": {"http_proxy": "wdasd","https_proxy": "fwef","prefer": 2},
	"proxy": "uyioooo","wpe": {"gl": true},"program_number": "ewqweqwe","multicast_iface": "efefefef","srt_mode": 1,
	"srt_key": {"passphrase": "qweewq","pbkeylen": 321},"rtmpsrc_type": 0,"webrtc": {"stun": "rewr","turn": "rew"},
	"programme": {"channel": "wdwdwd"}}`),
		},
		{
			name: "not correct field user_agent1",
			testArray: []byte(`{"user_agent": -2,"stream_link": {"http_proxy": "wdasd","https_proxy": "fwef","prefer": 2},
	"proxy": "uyioooo","wpe": {"gl": true},"program_number": "ewqweqwe","multicast_iface": "efefefef","srt_mode": 1,
	"srt_key": {"passphrase": "qweewq","pbkeylen": 321},"rtmpsrc_type": 0,"webrtc": {"stun": "rewr","turn": "rew"},
	"programme": {"channel": "wdwdwd"}}`),
		},
		{
			name: "not correct field prefer",
			testArray: []byte(`{"user_agent": 3,"stream_link": {"http_proxy": "wdasd","https_proxy": "fwef","prefer": -1},
	"proxy": "uyioooo","wpe": {"gl": true},"program_number": "ewqweqwe","multicast_iface": "efefefef","srt_mode": 1,
	"srt_key": {"passphrase": "qweewq","pbkeylen": 321},"rtmpsrc_type": 0,"webrtc": {"stun": "rewr","turn": "rew"},
	"programme": {"channel": "wdwdwd"}}`),
		},
		{
			name: "not correct field prefer1",
			testArray: []byte(`{"user_agent": 3,"stream_link": {"http_proxy": "wdasd","https_proxy": "fwef","prefer": 4},
	"proxy": "uyioooo","wpe": {"gl": true},"program_number": "ewqweqwe","multicast_iface": "efefefef","srt_mode": 1,
	"srt_key": {"passphrase": "qweewq","pbkeylen": 321},"rtmpsrc_type": 0,"webrtc": {"stun": "rewr","turn": "rew"},
	"programme": {"channel": "wdwdwd"}}`),
		},
		{
			name: "not correct field rtmpsrc_type",
			testArray: []byte(`{"user_agent": 3,"stream_link": {"http_proxy": "wdasd","https_proxy": "fwef","prefer": 2},
	"proxy": "uyioooo","wpe": {"gl": true},"program_number": "ewqweqwe","multicast_iface": "efefefef","srt_mode": 1,
	"srt_key": {"passphrase": "qweewq","pbkeylen": 321},"rtmpsrc_type": -1,"webrtc": {"stun": "rewr","turn": "rew"},
	"programme": {"channel": "wdwdwd"}}`),
		},
		{
			name: "not correct field rtmpsrc_type2",
			testArray: []byte(`{"user_agent": 3,"stream_link": {"http_proxy": "wdasd","https_proxy": "fwef","prefer": 2},
	"proxy": "uyioooo","wpe": {"gl": true},"program_number": "ewqweqwe","multicast_iface": "efefefef","srt_mode": 1,
	"srt_key": {"passphrase": "qweewq","pbkeylen": 321},"rtmpsrc_type": 2,"webrtc": {"stun": "rewr","turn": "rew"},
	"programme": {"channel": "wdwdwd"}}`),
		},
		{
			name: "not correct field srt_mode",
			testArray: []byte(`{"user_agent": 3,"stream_link": {"http_proxy": "wdasd","https_proxy": "fwef","prefer": 2},
	"proxy": "uyioooo","wpe": {"gl": true},"program_number": "ewqweqwe","multicast_iface": "efefefef","srt_mode": -1,
	"srt_key": {"passphrase": "qweewq","pbkeylen": 321},"rtmpsrc_type": 0,"webrtc": {"stun": "rewr","turn": "rew"},
	"programme": {"channel": "wdwdwd"}}`),
		},
		{
			name: "not correct field srt_mode",
			testArray: []byte(`{"user_agent": 3,"stream_link": {"http_proxy": "wdasd","https_proxy": "fwef","prefer": 2},
	"proxy": "uyioooo","wpe": {"gl": true},"program_number": "ewqweqwe","multicast_iface": "efefefef","srt_mode": 5,
	"srt_key": {"passphrase": "qweewq","pbkeylen": 321},"rtmpsrc_type": 0,"webrtc": {"stun": "rewr","turn": "rew"},
	"programme": {"channel": "wdwdwd"}}`),
		},
	}

	for _, obj := range testCases13 {
		t.Run(obj.name, func(t *testing.T) {
			assert.Error(t, json.Unmarshal(obj.testArray, &testVariable14))
		})
	}

	// type GetConfigJsonStreamRequest
	testVariable15 := media.GetConfigJsonStreamRequest{}
	testJSON = []byte(`{"id":"123ewe", "feedback_directory": "qwer","path":"C/wwdwd/ewfef"}`)
	err = json.Unmarshal(testJSON, &testVariable15)
	assert.NoError(t, err)
	assert.Equal(t, testVariable15.Sid, media.StreamId("123ewe"))
	assert.Equal(t, testVariable15.FeedbackDir, "qwer")
	assert.Equal(t, testVariable15.Path, "C/wwdwd/ewfef")

	testCases15 := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "nil id",
			testArray: []byte(`{"feedback_directory": "qwer","path":"C/wwdwd/ewfef"}`),
		},
		{
			name:      "empty id",
			testArray: []byte(`{"id":"", "feedback_directory": "qwer","path":"C/wwdwd/ewfef"}`),
		},
		{
			name:      "nil feedback_directory",
			testArray: []byte(`{"id":"123ewe","path":"C/wwdwd/ewfef"}`),
		},
		{
			name:      "empty feedback_directory",
			testArray: []byte(`{"id":"123ewe", "feedback_directory": "","path":"C/wwdwd/ewfef"}`),
		},
		{
			name:      "nil path",
			testArray: []byte(`{"id":"123ewe", "feedback_directory": "qwer"}`),
		},
		{
			name:      "empty path",
			testArray: []byte(`{"id":"123ewe", "feedback_directory": "qwer","path":""}`),
		},
	}

	for _, obj := range testCases15 {
		t.Run(obj.name, func(t *testing.T) {
			assert.Error(t, json.Unmarshal(obj.testArray, &testVariable15))
		})
	}

	// type GetPipelineStreamRequest
	testVariable16 := media.GetPipelineStreamRequest{}
	testJSON = []byte(`{"id":"123ewe", "feedback_directory": "qwer","path":"C/wwdwd/ewfef"}`)
	err = json.Unmarshal(testJSON, &testVariable16)
	assert.NoError(t, err)
	assert.Equal(t, testVariable16.Sid, media.StreamId("123ewe"))
	assert.Equal(t, testVariable16.FeedbackDir, "qwer")
	assert.Equal(t, testVariable16.Path, "C/wwdwd/ewfef")

	testCases16 := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "nil id",
			testArray: []byte(`{"feedback_directory": "qwer","path":"C/wwdwd/ewfef"}`),
		},
		{
			name:      "empty id",
			testArray: []byte(`{"id":"", "feedback_directory": "qwer","path":"C/wwdwd/ewfef"}`),
		},
		{
			name:      "nil feedback_directory",
			testArray: []byte(`{"id":"123ewe","path":"C/wwdwd/ewfef"}`),
		},
		{
			name:      "empty feedback_directory",
			testArray: []byte(`{"id":"123ewe", "feedback_directory": "","path":"C/wwdwd/ewfef"}`),
		},
		{
			name:      "nil path",
			testArray: []byte(`{"id":"123ewe", "feedback_directory": "qwer"}`),
		},
		{
			name:      "empty path",
			testArray: []byte(`{"id":"123ewe", "feedback_directory": "qwer","path":""}`),
		},
	}

	for _, obj := range testCases16 {
		t.Run(obj.name, func(t *testing.T) {
			assert.Error(t, json.Unmarshal(obj.testArray, &testVariable16))
		})
	}

	// type GetLogStreamRequest
	testVariable17 := media.GetLogStreamRequest{}
	testJSON = []byte(`{"id":"123ewe", "feedback_directory": "qwer","path":"C/wwdwd/ewfef"}`)
	err = json.Unmarshal(testJSON, &testVariable17)
	assert.NoError(t, err)
	assert.Equal(t, testVariable17.Sid, media.StreamId("123ewe"))
	assert.Equal(t, testVariable17.FeedbackDir, "qwer")
	assert.Equal(t, testVariable17.Path, "C/wwdwd/ewfef")

	testCases17 := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "nil id",
			testArray: []byte(`{"feedback_directory": "qwer","path":"C/wwdwd/ewfef"}`),
		},
		{
			name:      "empty id",
			testArray: []byte(`{"id":"", "feedback_directory": "qwer","path":"C/wwdwd/ewfef"}`),
		},
		{
			name:      "nil feedback_directory",
			testArray: []byte(`{"id":"123ewe","path":"C/wwdwd/ewfef"}`),
		},
		{
			name:      "empty feedback_directory",
			testArray: []byte(`{"id":"123ewe", "feedback_directory": "","path":"C/wwdwd/ewfef"}`),
		},
		{
			name:      "nil path",
			testArray: []byte(`{"id":"123ewe", "feedback_directory": "qwer"}`),
		},
		{
			name:      "empty path",
			testArray: []byte(`{"id":"123ewe", "feedback_directory": "qwer","path":""}`),
		},
	}

	for _, obj := range testCases17 {
		t.Run(obj.name, func(t *testing.T) {
			assert.Error(t, json.Unmarshal(obj.testArray, &testVariable17))
		})
	}

	// type OutputUriData
	testVariable99 := media.OutputUriData{}
	testJSON = []byte(`{"http_root":"123ewe", "chunk_duration": 23,"hls_type":1,"hlssink_type":0,"srt_mode":1,
	"playlist_root":"wfegg","rtmp_type":3,"rtmp_web_url":"gtg4h","rtmpsink_type":0,"kvs":{"stream_name":"werewr"},
	"azure":{"account_name":"nameAcc"},"webrtc":{"stun":"wer","turn":"grgreg"}}`)
	err = json.Unmarshal(testJSON, &testVariable99)
	assert.NoError(t, err)
	assert.Equal(t, *testVariable99.HttpRoot, "123ewe")
	assert.Equal(t, *testVariable99.ChunkDuration, 23)
	assert.Equal(t, *testVariable99.HlsType, media.HLS_PUSH)
	assert.Equal(t, *testVariable99.HlsSinkType, media.HLSSINK)
	assert.Equal(t, *testVariable99.SrtMode, media.CALLER)
	assert.Equal(t, *testVariable99.PlaylistRoot, "wfegg")
	assert.Equal(t, *testVariable99.RtmpType, 3)
	assert.Equal(t, *testVariable99.RtmpWebUrl, "gtg4h")
	assert.Equal(t, *testVariable99.RtmpSinkType, media.RTMPSINK)
	assert.Equal(t, *testVariable99.Kvs, media.KvsProp{StreamName: "werewr"})
	assert.Equal(t, *testVariable99.Azure, media.AzureProp{AccountName: "nameAcc"})
	assert.Equal(t, *testVariable99.WebRTC, media.WebRTCProp{Stun: "wer", Turn: "grgreg"})

	testCases99 := []struct {
		name      string
		testArray []byte
	}{
		{
			name: "invalid hls_type",
			testArray: []byte(`{"http_root":"123ewe", "chunk_duration": 23,"hls_type":1,"hlssink_type":-1,"srt_mode":1,
	"playlist_root":"wfegg","rtmp_type":3,"rtmp_web_url":"gtg4h","rtmpsink_type":0,"kvs":{"stream_name":"werewr"},
	"azure":{"account_name":"nameAcc"},"webrtc":{"stun":"wer","turn":"grgreg"}}`),
		},
		{
			name: "invalid hls_type",
			testArray: []byte(`{"http_root":"123ewe", "chunk_duration": 23,"hls_type":1,"hlssink_type":3,"srt_mode":1,
	"playlist_root":"wfegg","rtmp_type":3,"rtmp_web_url":"gtg4h","rtmpsink_type":0,"kvs":{"stream_name":"werewr"},
	"azure":{"account_name":"nameAcc"},"webrtc":{"stun":"wer","turn":"grgreg"}}`),
		},
		{
			name: "invalid hlssink_type",
			testArray: []byte(`{"http_root":"123ewe", "chunk_duration": 23,"hls_type":1,"hlssink_type":-1,"srt_mode":1,
	"playlist_root":"wfegg","rtmp_type":3,"rtmp_web_url":"gtg4h","rtmpsink_type":0,"kvs":{"stream_name":"werewr"},
	"azure":{"account_name":"nameAcc"},"webrtc":{"stun":"wer","turn":"grgreg"}}`),
		},
		{
			name: "invalid hlssink_type",
			testArray: []byte(`{"http_root":"123ewe", "chunk_duration": 23,"hls_type":1,"hlssink_type":5,"srt_mode":1,
	"playlist_root":"wfegg","rtmp_type":3,"rtmp_web_url":"gtg4h","rtmpsink_type":0,"kvs":{"stream_name":"werewr"},
	"azure":{"account_name":"nameAcc"},"webrtc":{"stun":"wer","turn":"grgreg"}}`),
		},
		{
			name: "invalid srt_mode",
			testArray: []byte(`{"http_root":"123ewe", "chunk_duration": 23,"hls_type":1,"hlssink_type":0,"srt_mode":-5,
	"playlist_root":"wfegg","rtmp_type":3,"rtmp_web_url":"gtg4h","rtmpsink_type":0,"kvs":{"stream_name":"werewr"},
	"azure":{"account_name":"nameAcc"},"webrtc":{"stun":"wer","turn":"grgreg"}}`),
		},
		{
			name: "invalid srt_mode",
			testArray: []byte(`{"http_root":"123ewe", "chunk_duration": 23,"hls_type":1,"hlssink_type":0,"srt_mode":10,
	"playlist_root":"wfegg","rtmp_type":3,"rtmp_web_url":"gtg4h","rtmpsink_type":0,"kvs":{"stream_name":"werewr"},
	"azure":{"account_name":"nameAcc"},"webrtc":{"stun":"wer","turn":"grgreg"}}`),
		},
		{
			name: "invalid rtmpsink_type",
			testArray: []byte(`{"http_root":"123ewe", "chunk_duration": 23,"hls_type":1,"hlssink_type":0,"srt_mode":1,
	"playlist_root":"wfegg","rtmp_type":3,"rtmp_web_url":"gtg4h","rtmpsink_type":50,"kvs":{"stream_name":"werewr"},
	"azure":{"account_name":"nameAcc"},"webrtc":{"stun":"wer","turn":"grgreg"}}`),
		},
		{
			name: "invalid rtmpsink_type",
			testArray: []byte(`{"http_root":"123ewe", "chunk_duration": 23,"hls_type":1,"hlssink_type":0,"srt_mode":1,
	"playlist_root":"wfegg","rtmp_type":3,"rtmp_web_url":"gtg4h","rtmpsink_type":-43,"kvs":{"stream_name":"werewr"},
	"azure":{"account_name":"nameAcc"},"webrtc":{"stun":"wer","turn":"grgreg"}}`),
		},
	}

	for _, obj := range testCases99 {
		t.Run(obj.name, func(t *testing.T) {
			assert.Error(t, json.Unmarshal(obj.testArray, &testVariable99))
		})
	}

}

func givPoint(char string) *string {
	return &char
}
