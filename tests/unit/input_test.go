package unit_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/fastogt/gofastocloud/media"
)

func TestInputUrl_Scheme(t *testing.T) {
	http := "http"
	https := "https"
	tests := []struct {
		name      string
		o         media.InputUrl
		want      *string
		assertion assert.ErrorAssertionFunc
	}{

		{name: "Valid",
			o:         media.InputUrl{Id: 1, Uri: "http://0.0.0.0:7000/master.m3u8"},
			want:      &http,
			assertion: assert.NoError,
		},
		{name: "Valid_2",
			o:         media.InputUrl{Id: 1, Uri: "https://0.0.0.0:7000/master.m3u8"},
			want:      &https,
			assertion: assert.NoError,
		},
		{name: "Error",
			o:         media.InputUrl{Id: 1, Uri: "0.0.0.0:7000/master.m3u8"},
			want:      nil,
			assertion: assert.Error,
		},
		{name: "Error_2",
			o:         media.InputUrl{Id: 1, Uri: "master.m3u8"},
			want:      nil,
			assertion: assert.Error,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.o.Scheme()
			assert.Equal(t, got, tt.want)
			tt.assertion(t, err)
		})
	}
}
