package unit_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/fastogt/gofastocloud/media"
)

func TestOutputUrl(t *testing.T) {
	sid := media.StreamId("some")

	out := media.MakeStandartHlsOutputUri("https://fastocloud.com", media.STREAM_TYPE_ENCODE, sid, 0, media.HLS_PULL, media.HLSSINK, true)

	stabled := out.StableForStreaming(media.STREAM_TYPE_ENCODE, sid)
	assert.Equal(t, out, stabled)
}

func TestOutputUrl_Scheme(t *testing.T) {
	http := "http"
	https := "https"
	tests := []struct {
		name      string
		o         media.OutputUrl
		want      *string
		assertion assert.ErrorAssertionFunc
	}{

		{name: "Valid",
			o:         media.OutputUrl{Id: 1, Uri: "http://0.0.0.0:7000/master.m3u8"},
			want:      &http,
			assertion: assert.NoError,
		},
		{name: "Valid_2",
			o:         media.OutputUrl{Id: 1, Uri: "https://0.0.0.0:7000/master.m3u8"},
			want:      &https,
			assertion: assert.NoError,
		},
		{name: "Error",
			o:         media.OutputUrl{Id: 1, Uri: "0.0.0.0:7000/master.m3u8"},
			want:      nil,
			assertion: assert.Error,
		},
		{name: "Error_2",
			o:         media.OutputUrl{Id: 1, Uri: "master.m3u8"},
			want:      nil,
			assertion: assert.Error,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.o.Scheme()
			assert.Equal(t, got, tt.want)
			tt.assertion(t, err)
		})
	}
}
