module gitlab.com/fastogt/gofastocloud

go 1.16

require (
	github.com/stretchr/testify v1.7.5
	gitlab.com/fastogt/gofastocloud_base v1.6.2
	gitlab.com/fastogt/gofastogt v1.3.0
)
