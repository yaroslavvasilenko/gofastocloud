package media

import (
	"encoding/json"
	"errors"
	"net/url"

	"gitlab.com/fastogt/gofastogt"
)

const kWebRTCInUrl = "unknown://webrtc"

type InputUrl struct {
	Id  int    `json:"id"`
	Uri string `json:"uri"`
}

func (i *InputUrl) Scheme() (*string, error) {
	u, err := url.Parse(i.Uri)
	if err != nil {
		return nil, err
	}
	if u.Scheme == "" {
		return nil, errors.New("url schema is required")
	}
	return &u.Scheme, nil
}

func (input *InputUrl) UnmarshalJSON(data []byte) error {
	required := struct {
		Id  *int    `json:"id"`
		Uri *string `json:"uri"`
	}{}
	err := json.Unmarshal(data, &required)
	if err != nil {
		return err
	}
	if required.Id == nil {
		return errors.New("id field required")
	}
	if *required.Id < 0 {
		return errors.New("invlaid id")
	}
	if required.Uri == nil {
		return errors.New("uri field required")
	}
	if len(*required.Uri) == 0 {
		return errors.New("invlaid uri")
	}

	input.Id = *required.Id
	input.Uri = *required.Uri
	return nil
}

// FIXME: need UnmarshalJSON
type InputUriData struct {
	// http
	UserAgent  *UserAgent  `bson:"user_agent,omitempty" json:"user_agent,omitempty"`
	StreamLink *StreamLink `bson:"stream_link,omitempty" json:"stream_link,omitempty"`
	Proxy      *string     `bson:"proxy,omitempty" json:"proxy,omitempty"`
	Wpe        *Wpe        `bson:"wpe,omitempty" json:"wpe,omitempty"`
	// udp
	ProgramNumber  *string `bson:"program_number,omitempty" json:"program_number,omitempty"`
	MulticastIface *string `bson:"multicast_iface,omitempty" json:"multicast_iface,omitempty"`
	// srt
	SrtMode *SrtMode `bson:"srt_mode,omitempty" json:"srt_mode,omitempty"`
	SrtKey  *SrtKey  `bson:"srt_key,omitempty" json:"srt_key,omitempty"`
	// rtmp
	RtmpSrcType *RtmpSrcType `bson:"rtmpsrc_type,omitempty" json:"rtmpsrc_type,omitempty"`
	// webrtc
	WebRTC *WebRTCProp `bson:"webrtc,omitempty" json:"webrtc,omitempty"`
	//
	Programme *Programme `bson:"programme,omitempty" json:"programme,omitempty"`
}

func (input *InputUriData) UnmarshalJSON(data []byte) error {
	req := struct {
		UserAgent      *UserAgent   `json:"user_agent,omitempty"`
		StreamLink     *StreamLink  `json:"stream_link,omitempty"`
		Proxy          *string      `json:"proxy,omitempty"`
		Wpe            *Wpe         `json:"wpe,omitempty"`
		ProgramNumber  *string      `json:"program_number,omitempty"`
		MulticastIface *string      `json:"multicast_iface,omitempty"`
		SrtMode        *SrtMode     `json:"srt_mode,omitempty"`
		SrtKey         *SrtKey      `json:"srt_key,omitempty"`
		RtmpSrcType    *RtmpSrcType `json:"rtmpsrc_type,omitempty"`
		WebRTC         *WebRTCProp  `json:"webrtc,omitempty"`
		Programme      *Programme   `json:"programme,omitempty"`
	}{}
	err := json.Unmarshal(data, &req)
	if err != nil {
		return err
	}
	if req.UserAgent != nil {
		if !req.UserAgent.IsValid() {
			return errors.New("not correct field user_agent")
		}
		input.UserAgent = req.UserAgent
	}
	if req.StreamLink != nil {
		if !req.StreamLink.Prefer.IsValid() {
			return errors.New("not correct field prefer")
		}
		input.StreamLink = req.StreamLink
	}
	if req.Proxy != nil {
		input.Proxy = req.Proxy
	}
	if req.Wpe != nil {
		input.Wpe = req.Wpe
	}
	if req.ProgramNumber != nil {
		input.ProgramNumber = req.ProgramNumber
	}
	if req.MulticastIface != nil {
		input.MulticastIface = req.MulticastIface
	}
	if req.SrtKey != nil {
		input.SrtKey = req.SrtKey
	}
	if req.RtmpSrcType != nil {
		if !req.RtmpSrcType.IsValid() {
			return errors.New("not correct field rtmpsrc_type")
		}
		input.RtmpSrcType = req.RtmpSrcType
	}
	if req.WebRTC != nil {
		input.WebRTC = req.WebRTC
	}
	if req.Programme != nil {
		input.Programme = req.Programme
	}
	if req.SrtMode != nil {
		if !req.SrtMode.IsValid() {
			return errors.New("not correct field srt_mode")
		}
		input.SrtMode = req.SrtMode
	}
	return nil
}

type InputUri struct {
	InputUrl     `bson:",inline"`
	InputUriData `bson:",inline"`
}

func (input *InputUri) UnmarshalJSON(data []byte) error {
	err := input.InputUrl.UnmarshalJSON(data)
	if err != nil {
		return err
	}

	err = json.Unmarshal(data, &input.InputUriData)
	if err != nil {
		return err
	}

	return nil
}

func NewWebRTCInputUri(oid int, prop *WebRTCProp) *InputUri {
	web := NewInputUri(oid, kWebRTCInUrl)
	web.WebRTC = prop
	return web
}

func MakeFileInputUri(oid int, path string) (*InputUri, error) {
	file, err := gofastogt.MakeFilePathProtocol(path)
	if err != nil {
		return nil, err
	}

	return NewInputUri(oid, file.GetFilePath()), nil
}

func NewInputUri(oid int, url string) *InputUri {
	uri := InputUrl{Id: oid, Uri: url}
	return &InputUri{InputUrl: uri}
}
