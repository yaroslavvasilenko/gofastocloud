package media

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
)

const kFakeOutUrl = "unknown://fake"
const kWebRTCOutUrl = "unknown://webrtc"

type OutputUrl struct {
	Id  int    `json:"id"`
	Uri string `json:"uri"`
}

func (o *OutputUrl) Scheme() (*string, error) {
	u, err := url.Parse(o.Uri)
	if err != nil {
		return nil, err
	}
	if u.Scheme == "" {
		return nil, errors.New("url schema is required")
	}
	return &u.Scheme, nil
}

func (output *OutputUrl) Origin() (*string, error) {
	u, err := url.Parse(output.Uri)
	if err != nil {
		return nil, err
	}

	result := fmt.Sprintf("%s://%s", u.Scheme, u.Host)
	return &result, nil
}

func (output *OutputUrl) UnmarshalJSON(data []byte) error {
	required := struct {
		Id  *int    `json:"id"`
		Uri *string `json:"uri"`
	}{}
	err := json.Unmarshal(data, &required)
	if err != nil {
		return err
	}
	if required.Id == nil {
		return errors.New("id field required")
	}
	if *required.Id < 0 {
		return errors.New("invlaid id")
	}
	if required.Uri == nil {
		return errors.New("uri field required")
	}
	if len(*required.Uri) == 0 {
		return errors.New("invlaid uri")
	}

	output.Id = *required.Id
	output.Uri = *required.Uri
	return nil
}

type OutputUriData struct {
	HttpRoot      *string       `json:"http_root,omitempty"`
	ChunkDuration *int          `json:"chunk_duration,omitempty"`
	HlsType       *HlsType      `json:"hls_type,omitempty"`
	HlsSinkType   *HlsSinkType  `json:"hlssink_type,omitempty"`
	SrtMode       *SrtMode      `json:"srt_mode,omitempty"`
	SrtKey        *SrtKey       `json:"srt_key,omitempty"`
	PlaylistRoot  *string       `json:"playlist_root,omitempty"`
	RtmpType      *int          `json:"rtmp_type,omitempty"`
	RtmpWebUrl    *string       `json:"rtmp_web_url,omitempty"`
	RtmpSinkType  *RtmpSinkType `json:"rtmpsink_type,omitempty"`
	Kvs           *KvsProp      `json:"kvs,omitempty"`
	Azure         *AzureProp    `json:"azure,omitempty"`
	WebRTC        *WebRTCProp   `json:"webrtc,omitempty"`
	Token         *bool         `json:"token,omitempty"`
}

func (output *OutputUriData) UnmarshalJSON(data []byte) error {
	req := struct {
		HttpRoot      *string       `json:"http_root,omitempty"`
		ChunkDuration *int          `json:"chunk_duration,omitempty"`
		HlsType       *HlsType      `json:"hls_type,omitempty"`
		HlsSinkType   *HlsSinkType  `json:"hlssink_type,omitempty"`
		SrtMode       *SrtMode      `json:"srt_mode,omitempty"`
		SrtKey        *SrtKey       `json:"srt_key,omitempty"`
		PlaylistRoot  *string       `json:"playlist_root,omitempty"`
		RtmpType      *int          `json:"rtmp_type,omitempty"`
		RtmpWebUrl    *string       `json:"rtmp_web_url,omitempty"`
		RtmpSinkType  *RtmpSinkType `json:"rtmpsink_type,omitempty"`
		Kvs           *KvsProp      `json:"kvs,omitempty"`
		Azure         *AzureProp    `json:"azure,omitempty"`
		WebRTC        *WebRTCProp   `json:"webrtc,omitempty"`
		Token         *bool         `json:"token,omitempty"`
	}{}
	err := json.Unmarshal(data, &req)
	if err != nil {
		return err
	}
	if req.HttpRoot != nil {
		output.HttpRoot = req.HttpRoot
	}
	if req.ChunkDuration != nil {
		output.ChunkDuration = req.ChunkDuration
	}
	if req.HlsType != nil {
		if !req.HlsType.IsValid() {
			return errors.New("not correct field hls_type")
		}
		output.HlsType = req.HlsType
	}
	if req.HlsSinkType != nil {
		if !req.HlsSinkType.IsValid() {
			return errors.New("not correct field hlssink_type")
		}
		output.HlsSinkType = req.HlsSinkType
	}
	if req.SrtMode != nil {
		if !req.SrtMode.IsValid() {
			return errors.New("not correct field srt_mode")
		}
		output.SrtMode = req.SrtMode
	}
	if req.SrtKey != nil {
		output.SrtKey = req.SrtKey
	}
	if req.PlaylistRoot != nil {
		output.PlaylistRoot = req.PlaylistRoot
	}
	if req.RtmpType != nil {
		output.RtmpType = req.RtmpType
	}
	if req.RtmpWebUrl != nil {
		output.RtmpWebUrl = req.RtmpWebUrl
	}
	if req.Kvs != nil {
		output.Kvs = req.Kvs
	}
	if req.Azure != nil {
		output.Azure = req.Azure
	}
	if req.WebRTC != nil {
		output.WebRTC = req.WebRTC
	}
	if req.Token != nil {
		output.Token = req.Token
	}
	if req.RtmpSinkType != nil {
		if !req.RtmpSinkType.IsValid() {
			return errors.New("not correct field rtmpsink_type")
		}
		output.RtmpSinkType = req.RtmpSinkType
	}

	return nil
}

type OutputUri struct {
	OutputUrl     `bson:",inline"`
	OutputUriData `bson:",inline"`
}

func (output *OutputUri) StableForStreaming(streamType StreamType, sid StreamId) OutputUri {
	if output.IsHls() {
		origin, err := output.Origin()
		if err != nil {
			return *output
		}

		var httpRoot string
		if streamType == STREAM_TYPE_VOD_ENCODE || streamType == STREAM_TYPE_VOD_RELAY || streamType == STREAM_TYPE_VOD_PROXY {
			httpRoot = MakeVodsHttpRoot(streamType, sid, output.Id)
		} else if streamType == STREAM_TYPE_COD_ENCODE || streamType == STREAM_TYPE_COD_RELAY {
			httpRoot = MakeCodsHttpRoot(streamType, sid, output.Id)
		} else if streamType == STREAM_TYPE_TIMESHIFT_RECORDER || streamType == STREAM_TYPE_TIMESHIFT_PLAYER {
			httpRoot = MakeCodsHttpRoot(streamType, sid, output.Id)
		} else {
			httpRoot = MakeHlsHttpRoot(streamType, sid, output.Id)
		}

		return MakeHlsOutputUri(*origin, streamType, sid, output.Id, httpRoot, *output.HlsType, *output.HlsSinkType, *output.Token)
	}

	return *output
}

func (output *OutputUri) UnmarshalJSON(data []byte) error {
	err := output.OutputUrl.UnmarshalJSON(data)
	if err != nil {
		return err
	}

	err = json.Unmarshal(data, &output.OutputUriData)
	if err != nil {
		return err
	}

	return nil
}

func (uri *OutputUri) IsHls() bool {
	if uri.HttpRoot == nil && uri.HlsSinkType == nil && uri.HlsType == nil {
		return false
	}
	return true
}

func NewWebRTCOutputUri(oid int, prop *WebRTCProp) *OutputUri {
	web := NewOutputUri(oid, kWebRTCOutUrl)
	web.WebRTC = prop
	return web
}

func NewFakeOutputUri(oid int) *OutputUri {
	return NewOutputUri(oid, kFakeOutUrl)
}

func NewOutputUri(oid int, uri string) *OutputUri {
	return &OutputUri{OutputUrl: OutputUrl{Id: oid, Uri: uri}}
}

func MakeHlsOutputUri(origin string, streamType StreamType, sid StreamId, oid int, httpRoot string, hlsType HlsType, hlsSinkType HlsSinkType, token bool) OutputUri {
	origin = stableOrigin(origin)
	out := OutputUrl{Id: oid, Uri: fmt.Sprintf("%s%s", origin, MakeHlsPostfix(streamType, sid, oid))}
	outData := OutputUriData{HttpRoot: &httpRoot, HlsType: &hlsType, HlsSinkType: &hlsSinkType, Token: &token}
	return OutputUri{OutputUrl: out, OutputUriData: outData}
}

func MakeHlsNginxOutputUri(origin string, streamType StreamType, sid StreamId, oid int, httpRoot string, hlsType HlsType, hlsSinkType HlsSinkType) OutputUri {
	origin = stableOrigin(origin)
	out := OutputUrl{Id: oid, Uri: fmt.Sprintf("%sfastocloud/hls/%s", origin, MakeHlsPostfix(streamType, sid, oid))}
	outData := OutputUriData{HttpRoot: &httpRoot, HlsType: &hlsType, HlsSinkType: &hlsSinkType}
	return OutputUri{OutputUrl: out, OutputUriData: outData}
}

func MakeVodsHlsOutputUri(origin string, streamType StreamType, sid StreamId, oid int, httpRoot string, hlsType HlsType, hlsSinkType HlsSinkType, token bool) OutputUri {
	origin = stableOrigin(origin)
	out := OutputUrl{Id: oid, Uri: fmt.Sprintf("%s%s", origin, MakeHlsPostfix(streamType, sid, oid))}
	outData := OutputUriData{HttpRoot: &httpRoot, HlsType: &hlsType, HlsSinkType: &hlsSinkType, Token: &token}
	return OutputUri{OutputUrl: out, OutputUriData: outData}
}

func MakeVodsHlsNginxOutputUri(origin string, streamType StreamType, sid StreamId, oid int, httpRoot string, hlsType HlsType, hlsSinkType HlsSinkType) OutputUri {
	origin = stableOrigin(origin)
	out := OutputUrl{Id: oid, Uri: fmt.Sprintf("%sfastocloud/vods/%s", origin, MakeHlsPostfix(streamType, sid, oid))}
	outData := OutputUriData{HttpRoot: &httpRoot, HlsType: &hlsType, HlsSinkType: &hlsSinkType}
	return OutputUri{OutputUrl: out, OutputUriData: outData}
}

func MakeHlsPostfix(streamType StreamType, sid StreamId, oid int) string {
	return fmt.Sprintf("%d/%s/%d/master.m3u8", streamType, sid, oid)
}

func stableOrigin(origin string) string {
	if origin[len(origin)-1] != '/' {
		origin += "/"
	}
	return origin
}
